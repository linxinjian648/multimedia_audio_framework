# Copyright (c) 2024 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/ohos.gni")
import("//build/test.gni")

module_output_path = "multimedia_audio_framework/audio_capturer_source"

ohos_unittest("remote_fast_audio_capturer_source_unittest") {
  module_out_path = module_output_path

  include_dirs = [
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/remote_fast/",
    "//foundation/multimedia/audio_framework/interfaces/inner_api/native/audiocommon/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/common/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/common/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/audioutils/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/devicemanager/interface/",
    "//foundation/multimedia/audio_framework/services/audio_policy/server/include/service/common/",
  ]

  sources = [ "src/remote_fast_audio_capturer_source_unittest.cpp" ]
  cflags = [
    "-Wall",
    "-Werror",
    "-fno-access-control",
  ]

  deps = [
    "../../../../audioutils:audio_utils",
    "../../../devicemanager:audio_device_manager",
    "../../../source:remote_fast_audio_capturer_source",
  ]

  external_deps = [
    "ability_base:want",
    "ability_runtime:abilitykit_native",
    "access_token:libaccesstoken_sdk",
    "access_token:libnativetoken",
    "access_token:libtoken_setproc",
    "audio_framework:audio_client",
    "audio_framework:audio_renderer",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "core_service:libtel_common",
    "core_service:tel_core_service_api",
    "drivers_interface_distributed_audio:libdaudio_proxy_1.0",
    "eventhandler:libeventhandler",
    "graphic_surface:surface",
    "hilog:libhilog",
    "ipc:ipc_single",
    "player_framework:system_sound_client",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]

  part_name = "audio_framework"
  subsystem_name = "multimedia"
}

ohos_unittest("remote_audio_capturer_source_unit_test") {
  module_out_path = module_output_path

  include_dirs = [
    "../../../../../../interfaces/inner_api/native/audiocommon/include",
    "//foundation/multimedia/audio_framework/interfaces/inner_api/native/audiocommon/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/common/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/common/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/remote/",
    "//foundation/multimedia/audio_framework/frameworks/native/audioutils/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/devicemanager/interface/",
    "//foundation/multimedia/audio_framework/services/audio_policy/server/include/service/common/",
  ]

  sources = [ "src/remote_audio_capturer_source_unit_test.cpp" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-fno-access-control",
  ]

  deps = [
    "../../../../audioutils:audio_utils",
    "../../../devicemanager:audio_device_manager",
    "../../../source:remote_audio_capturer_source",
  ]

  external_deps = [
    "c_utils:utils",
    "drivers_interface_audio:libeffect_proxy_1.0",
    "googletest:gmock",
    "googletest:gtest",
    "hilog:libhilog",
    "ipc:ipc_single",
    "pulseaudio:pulse",
  ]

  part_name = "audio_framework"
  subsystem_name = "multimedia"
}

ohos_unittest("audio_capturer_source_unit_test") {
  module_out_path = module_output_path

  include_dirs = [
    "../../../../../../interfaces/inner_api/native/audiocommon/include",
    "//foundation/multimedia/audio_framework/interfaces/inner_api/native/audiocommon/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/common/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/common/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/primary/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/fast/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/source/bluetooth/",
    "//foundation/multimedia/audio_framework/frameworks/native/audioutils/include/",
    "//foundation/multimedia/audio_framework/frameworks/native/hdiadapter/devicemanager/interface/",
    "//foundation/multimedia/audio_framework/services/audio_policy/server/include/service/common/",
    "./include",
  ]

  sources = [ "src/audio_capturer_source_unit_test.cpp" ]

  cflags = [
    "-Wall",
    "-Werror",
    "-fno-access-control",
  ]

  deps = [
    "../../../../audioutils:audio_utils",
    "../../../devicemanager:audio_device_manager",
    "../../../source:audio_capturer_source",
    "../../../source:bluetooth_capturer_source",
    "../../../source:fast_audio_capturer_source",
  ]

  external_deps = [
    "c_utils:utils",
    "drivers_interface_audio:libeffect_proxy_1.0",
    "googletest:gmock",
    "googletest:gtest",
    "hilog:libhilog",
    "ipc:ipc_single",
    "pulseaudio:pulse",
  ]

  part_name = "audio_framework"
  subsystem_name = "multimedia"
}
